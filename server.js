'use strict';

const express = require('express');
const fs = require('fs');


// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

var funnels = {};

// App
const app = express();
app.use(express.json({limit: '50mb'}));

app.get('/api/v1/funnels/:funnelId', (req, res) => {
    var funnelId = req.params.funnelId;
    console.log("GET request with funnelId: " + funnelId);
    res.set('Content-Type', 'application/json');
    res.send(funnels[funnelId])
});
app.post('/api/v1/funnels', (req, res) => {
    var funnel = req.body;
    console.log("POST request with funnelId: " + funnel.instanceId);
    var funnel = req.body;
    funnels[funnel.instanceId] = funnel;
    res.status(200).end();
});
app.patch('/api/v1/funnels/:funnelId', (req, res) => {
    var funnelId = req.params.funnelId;
    console.log("PATCH request with funnelId: " + funnelId);
    var funnel = req.body;
    if (funnel.data.dossiers != null && funnel.data.dossiers.items != null) {
//      console.log("updating dossiers with: " + JSON.stringify(funnel.data.dossiers))
      for (var dossierId in funnel.data.dossiers.items) {
        console.log("update dossier with id: " + dossierId);
        funnels[funnelId].data.dossiers.items[dossierId] = funnel.data.dossiers.items[dossierId];
      }
    }
    if (funnel.data.documentsV2 != null && funnel.data.documentsV2.items != null) {
//      console.log("updating documents with: " + JSON.stringify(funnel.data.documentsV2))
      for (var documentId in funnel.data.documentsV2.items) {
        console.log("update document with id: " + documentId);
        funnels[funnelId].data.documentsV2.items[documentId] = funnel.data.documentsV2.items[documentId];
      }
    }
    if (funnel.data['signature-timestamp'] != null) {
      console.log("updating signature-timestamp with: " + JSON.stringify(funnel.data['signature-timestamp']))
      funnels[funnelId].data['signature-timestamp'] = funnel.data['signature-timestamp'];
    }
    if (funnel.data.customers != null && funnel.data.customers.items != null) {
//      console.log("updating customers with: " + JSON.stringify(funnel.data.customers))
      for (var customerId in funnel.data.customers.items) {
        console.log("update customer with id: " + customerId);
        funnels[funnelId].data.customers.items[customerId] = funnel.data.customers.items[customerId];
      }
    }
    if (funnel.data.dossierRejections != null) {
      console.log("updating dossierRejections with: " + JSON.stringify(funnel.data.dossierRejections))
      funnels[funnelId].data.dossierRejections = funnel.data.dossierRejections;
    }
   if (funnel.data['digital-signature'] != null) {
      //console.log("updating digital-signature with: " + JSON.stringify(funnel.data['digital-signature']))
      console.log("updating digital-signature")
      funnels[funnelId].data['digital-signature'] = funnel.data['digital-signature'];
   }
    if (funnel.data.wishlist != null && funnel.data.wishlist.items != null) {
//      console.log("updating customers with: " + JSON.stringify(funnel.data.customers))
      for (var wishlistId in funnel.data.wishlist.items) {
        console.log("update wishlist with id: " + wishlistId);
        funnels[funnelId].data.wishlist.items[wishlistId] = funnel.data.wishlist.items[wishlistId];
      }
    }
    res.status(200).end();
});
app.put('/api/v1/funnels/:funnelId', (req, res) => {
    var funnelId = req.params.funnelId;
    console.log("PUT request with funnelId: " + funnelId);
    var funnel = req.body;
    funnels[funnelId].data.customers = funnel.data.customers;
    funnels[funnelId].data.dossierRejections = funnel.data.dossierRejections;
    if (funnel.data.dossiers != null)
      funnels[funnelId].data.dossiers = funnel.data.dossiers;
//    if (funnel.data.activations != null)
//      funnels[funnelId].data.activations = funnel.data.activations;
    if (funnel.data.activationsPre != null)
      funnels[funnelId].data.activationsPre = funnel.data.activationsPre;
    if (funnel.data.identities != null)
      funnels[funnelId].data.identities = funnel.data.identities;
    res.status(200).end();
});
app.put('/api/v1/funnels/:funnelId/steps/:stepId/complete', (req, res) => {
    var funnelId = req.params.funnelId;
    console.log("COMPLETE request with funnelId: " + funnelId);
    res.set('Content-Type', 'application/json');
    res.send('{ "uid": "62bb6d34-bd83-4e2b-a141-4ceb4b24488b", "creationDate": "2022-03-09T17:00:03.608+0000", "lastEdit": "2022-03-09T17:00:03.608+0000", "steps": [ { "stepId": "catalog-new", "stepName": "Catalogo Prodotti", "uid": "59fd365d-98a5-48cd-80dd-4c3c827f0941", "level": 50, "priority": 50, "disallowGoBack": false, "allowDynamicNavigation": false, "gate": false, "enabled": true, "hidden": false, "internal": true, "running": false, "endpoint": "/catalog-new", "state": "new", "success": false, "completed": false, "productIds": [], "dependsOnSteps": [], "dependsOnLevels": [] } ], "executedUids": [], "nextUids": [ "59fd365d-98a5-48cd-80dd-4c3c827f0941" ], "nextStepUrl": "/catalog-new", "navigationInfo": { "nextStepToCompleteUrl": "/catalog-new", "lastGateStepLevel": 0 }, "byProduct": {} }')
});

// attachments
app.get('/api/v1/funnels/:funnelId/attachment/:attachmentId', (req, res) => {
        var funnelId = req.params.funnelId;
        console.log("ATTACHMENT request with funnelId: " + funnelId);
        res.set('Content-Type', 'application/json');
        res.send('{"id":"df980a0d-3797-47d2-a179-6a0f01d71f20","filename":"prova_1.pdf","description":"File test 1","url":"http://localhost:8090/attachment-cloud/27/27b447c2-9507-42f9-9c94-f8fd98103472/prova_1.pdf?se=2022-11-25T13%3A45%3A07Z&sig=bw4cvsGcp0hJCnKLOV3jRH4t39kdFpP4EKl4Htgmpik%3D&sp=r&spr=https&srt=co&ss=b&sv=2020-04-08","repository":"azuresa"}')
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);